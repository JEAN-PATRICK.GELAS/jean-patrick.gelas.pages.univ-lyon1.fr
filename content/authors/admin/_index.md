---
# Display name
title: Jean-Patrick Gelas

# Name pronunciation (optional)
name_pronunciation: 

# Full name (for SEO)
first_name: Jean-Patrick
last_name: Gelas

# Status emoji
status:
  icon: ⌨️

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Associate Professor (MCF, HC)

# Organizations/Affiliations to show in About widget
organizations:
  - name: LIRIS / Team SOC
    url: https://liris.cnrs.fr/equipe/soc
  - name: Université Claude Bernard - Lyon 1
    url: https://www.univ-lyon1.fr

# Short bio (displayed in user profile at end of posts)
bio: '' 
# My research interests are ...

# Interests to show in About widget
interests:
  - Blockchain technologies
  - Computer networks
  - Distributed system
  - Energy efficiency


# Education to show in About widget
education:
  courses:
    - course: PhD in Computer Science
      institution: INRIA / ENS Lyon / UCB Lyon 1
      year: 2003
    - course: DEA Informatique Fondamentale
      institution: ENS Lyon
      year: 2000
    

# Skills
# For available icons, see: https://docs.hugoblox.com/getting-started/page-builder/#icons
skills:
  - name: Technical
    items:
      - name: Python
        description: ''
        percent: 80
        icon: python
        icon_pack: fab
      - name: Data Science
        description: ''
        percent: 100
        icon: chart-line
        icon_pack: fas
      - name: SQL
        description: ''
        percent: 40
        icon: database
        icon_pack: fas
  - name: Hobbies
    color: '#eeac02'
    color_border: '#f0bf23'
    items:
      - name: Hiking
        description: ''
        percent: 60
        icon: person-hiking
        icon_pack: fas
      - name: Cats
        description: ''
        percent: 100
        icon: cat
        icon_pack: fas
      - name: Photography
        description: ''
        percent: 80
        icon: camera-retro
        icon_pack: fas

# Social/Academic Networking
# For available icons, see: https://docs.hugoblox.com/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: '/#contact'
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/jpgelas
    label: Follow me on Twitter
    display:
      header: false
  - icon: graduation-cap # Alternatively, use `google-scholar` icon from `ai` icon pack
    icon_pack: fas
    link: https://scholar.google.co.uk/citations?user=NMd2LuIAAAAJ
  - icon: github
    icon_pack: fab
    link: https://github.com/jpgelas
  - icon: linkedin
    icon_pack: fab
    link: https://www.linkedin.com/in/jean-patrick-gelas/
  # Link to a PDF of your resume/CV.
  # To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.yaml`,
  # and uncomment the lines below.
  - icon: cv
    icon_pack: ai
    link: uploads/resume.pdf

# Highlight the author in author lists? (true/false)
highlight_name: true
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
{style="text-align: justify;"}
