---
title: Enseignement
layout: docs # Do not modify.

# Optional header image (relative to `static/img/` folder).
header:
  caption: ""
  image: ""
---

Enseignements en [licence d'informatique](http://licence-info.univ-lyon1.fr/), [master 1 informatique](http://master-info.univ-lyon1.fr/M1/), master 2 parcours [Technologies de l'Information et Web](http://master-info.univ-lyon1.fr/TIW/).

## UEs sur d'autres sites Web

[LIFPF: Programmation fonctionnelle](https://forge.univ-lyon1.fr/programmation-fonctionnelle/lifpf/-/blob/main/README.md)

[MIF24: Bases de données NoSQL](https://forge.univ-lyon1.fr/mif24-bdnosql/mif24-bdnosql)

[TIW-IS: Intergiciels et services](https://forge.univ-lyon1.fr/tiw-is/tiw-is-2022-2023)

## UEs sur ce site Web

[Tomuss](https://tomus.univ-lyon1.fr)

- Séance 1 & 2: [Diapositives]({{< ref "/slides/example" >}})
