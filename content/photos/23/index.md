---
title: 'Promotion 2023-2024'
date: '2023-12-01T00:00:00Z'
summary: 'Photos souvenir de la remise des diplômes des étudiants de L3 et de M2'
profile : false
share: false
---

**Should be available end of december 2024**

{{< gallery album="promo_2023-2024" >}}


