---
title: 'Promotion M2 CCI 2024-2025'
date: '2024-12-13T00:00:00Z'
summary: 'Photos de la promotion de M2CCI 2024-2025 prisent juste avant les fêtes de Noël.'

profile : false
share: false

---

{{< gallery album="promo_cci_2024-2025" >}}

