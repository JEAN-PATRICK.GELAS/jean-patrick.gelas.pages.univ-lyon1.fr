---
title: 'Promotion 2022-2023'
date: '2023-12-01T00:00:00Z'
summary: 'Photos souvenir de la remise des diplômes des étudiants de L3 et de M2'

profile : false
share: false

---

{{< gallery album="promo_2022-2023" >}}
